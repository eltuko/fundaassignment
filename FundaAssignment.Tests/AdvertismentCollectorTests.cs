﻿using System;
using System.Linq;
using NUnit.Framework;

namespace FundaAssignment.Tests
{
    [TestFixture]
    public class AdvertismentCollectorTests
    {
        private readonly string _partnerApiBaseUrl = "http://partnerapi.funda.nl/feeds/Aanbod.svc";
        private readonly string _validApikey = "005e7c1d6f6c4f9bacac16760286e3cd";

        [Test]
        public void Should_return_no_result_if_nothing_was_found()
        {
            var sut = new AdvertismentCollector(baseuri: _partnerApiBaseUrl, apikey: _validApikey);

            var result = sut.GetSearchResult(page: 1, pagesize: 1, searchquery: "/wronglocation/");

            Assert.That(result.Objects.Count(), Is.EqualTo(0));
        }

        [Test]
        public void Should_throw_an_exception_if_key_is_wrong()
        {
            var sut = new AdvertismentCollector(baseuri: _partnerApiBaseUrl, apikey: "wrongkey");

            var exception = Assert.Throws<AggregateException>(() => sut.GetSearchResult(page: 1, pagesize: 1, searchquery: "/amsterdam/"));

            Assert.That(exception.InnerExceptions.First(), Is.TypeOf<UnauthorizedAccessException>());
            Assert.That(exception.InnerExceptions.First().Message, Is.EqualTo("Attempted to perform an unauthorized operation."));
        }

        [Test]
        public void Should_throw_an_exception_on_a_wrong_request()
        {
            var sut = new AdvertismentCollector(baseuri: "http://partnerapi.funda.nl/feeds/WrongURL.svc", apikey: _validApikey);

            var exception = Assert.Throws<AggregateException>(() => sut.GetSearchResult(page: 0, pagesize: 0, searchquery: "/wrong/"));

            Assert.That(exception.InnerExceptions.First(), Is.TypeOf<Exception>());
            Assert.That(exception.InnerExceptions.First().Message, Is.EqualTo("Unhandled exception."));
        }

        [Test]
        public void Should_get_the_first_ad_available()
        {
            var sut = new AdvertismentCollector(baseuri: _partnerApiBaseUrl, apikey: _validApikey);

            var result = sut.GetSearchResult(page: 1, pagesize: 1, searchquery: "/amsterdam/tuin/").Objects.First();

            // these values will change in time. Didn't want to mock objects for this assignment to not make to heavy :)
            Assert.That(result.Id, Is.EqualTo(new Guid("17514cba-f712-428c-936b-ce336861d7c3"))); 
            Assert.That(result.MakelaarId, Is.EqualTo(24594));
        }

        [Test]
        public void Should_get_more_adds()
        {
            var sut = new AdvertismentCollector(baseuri: _partnerApiBaseUrl, apikey: _validApikey);

            var result = sut.GetSearchResult(page: 1, pagesize: 10, searchquery: "/amsterdam/tuin/").Objects;

            Assert.That(result.Count(), Is.EqualTo(10));
        }

        [Test]
        public void Should_get_top_10_maakelars_offering_adds_in_amsterdam()
        {
            var sut = new AdvertismentCollector(baseuri: _partnerApiBaseUrl, apikey: _validApikey);

            var result = sut.GetTopTenMakelaars(searchquery: "/amsterdam/");

            // Some nice output as a demo
            Console.WriteLine("List of makelaars that have most advertisements in amsterdam:");
            Console.WriteLine(result);

            //example result:
            //List of makelaars that have most advertisements in amsterdam:
            //77  24614   Nieuw West Makelaardij B.V.
            //77  24605   Hallie & Van Klooster Makelaardij
            //73  12285   Makelaarsland
            //55  24594   Hoekstra en van Eck Amsterdam Noord
            //53  24079   Makelaardij Van der Linden Amsterdam
            //53  24067   Broersma Makelaardij
            //52  60557   Linger OG
            //51  24783   Hoekstra en van Eck Amsterdam West
            //40  24648   Heeren Makelaars
            //40  24661   iBlue Makelaars
        }

        [Test]
        public void Should_get_top_10_maakelars_offering_adds_in_amsterdam_with_a_garden()
        {
            var sut = new AdvertismentCollector(baseuri: _partnerApiBaseUrl, apikey: _validApikey);

            var result = sut.GetTopTenMakelaars(searchquery: "/amsterdam/tuin/");

            // Some nice output as a demo
            Console.WriteLine("List of makelaars that have most advertisements in amsterdam with a garden:");
            Console.WriteLine(result);

            //example result:
            //List of makelaars that have most advertisements in amsterdam with a garden:
            //34	24594	Hoekstra en van Eck Amsterdam Noord
            //26	24614	Nieuw West Makelaardij B.V.
            //19	24067	Broersma Makelaardij
            //17	12285	Makelaarsland
            //17	60557	Linger OG
            //13	24763	RET Makelaars
            //13	24648	Heeren Makelaars
            //13	24079	Makelaardij Van der Linden Amsterdam
            //10	24461	ERA Van De Steege
            //10	19317	Makelaar1 Internetmakelaar             
        }
    }
}