using System.Collections.Generic;

namespace FundaAssignment
{
    public class AdvertismentSearchResult
    {
        public IEnumerable<Advertisment> Objects { get; set; }
        public Paging Paging { get; set; }
    }
}