﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace FundaAssignment
{
    // This class helps to collect advertisments for further processing
    public class AdvertismentCollector
    {
        private readonly string _partnerApiUrl;

        public AdvertismentCollector(string baseuri, string apikey)
        {
            // build the base url
            _partnerApiUrl = $"{baseuri}/json/{apikey}";
        }

        public AdvertismentSearchResult GetSearchResult(int page, int pagesize, string searchquery)
        {
            // build the api url
            var apiUrl = $"{_partnerApiUrl}/?type=koop&zo={searchquery}&page={page}&pagesize={pagesize}";
            // raw call to api, it is advisable here to implement retries with circuitbreaker
            var payload = GetAdvertismentsPayload(apiUrl).Result;
            // return the deserialized object
            return JsonConvert.DeserializeObject<AdvertismentSearchResult>(payload);
        }

        // raw http call to api
        private async Task<string> GetAdvertismentsPayload(string url)
        {
            using (var client = new HttpClient())
            {
                HttpResponseMessage response = await client.GetAsync(url);

                if (!response.IsSuccessStatusCode)
                {
                    switch (response.StatusCode)
                    {
                        case HttpStatusCode.Unauthorized:

                            throw new UnauthorizedAccessException();
                        default:
                            throw new Exception("Unhandled exception.");
                    }
                }

                return await response.Content.ReadAsStringAsync();
            }
        }

        public string GetTopTenMakelaars(string searchquery)
        {
            var page = 1;
            var pageSize = 25;
            var adverstisments = new List<Advertisment>();

            // first we collect all the advertisments
            while (true)
            {
                var searchresult = GetSearchResult(page, pageSize, searchquery);

                adverstisments.AddRange(searchresult.Objects);

                if (searchresult.Paging.VolgendeUrl == null) // if VolgendeUrl is null we're at the end. In alternative we could also page by using this url. This would also be advised in fact to avoid paging issues.
                    break;

                page++;
            }

            // here we group them by MakelaarId keeping in mind the occurrences
            var query = from ad in adverstisments
                        group ad by ad.MakelaarId into g
                        orderby g.Count() descending
                        select new { MakelaarId = g.Key, Count = g.Count(), g.First().MakelaarNaam };

            // print out the top10 in a report. In alternative we could build an object to return
            var builder = new StringBuilder();

            foreach (var item in query.Where(s => s.MakelaarId != 0).Take(10)) // filter out MakelaarId with value 0. The assumption is that we don't know the makelaar for this ad
            {
                builder.AppendLine($"{item.Count}\t{item.MakelaarId}\t{item.MakelaarNaam}");
            }

            return builder.ToString();
        }
    }
}