using System;

namespace FundaAssignment
{
    public class Advertisment
    {
        public Guid Id { get; set; }
        public decimal MakelaarId { get; set; }
        public string MakelaarNaam { get; set; }
    }
}